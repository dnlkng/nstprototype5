package de.nst.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity
public class Person{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Version
    @Column(name = "version")
    private Integer version;

    @Column(name = "vorname")
    private String vorname;

    @Column(name = "nachname")
    private String nachname;

    private Date gebDatum;

    @OneToMany
    private List<Adresse> adressen;

    public void setAdressen(List<Adresse> a){
        this.adressen = a;
    }

    public List<Adresse> getAdressen(){
        return adressen;
    }

    public void setGebDatum(Date d){
    	this.gebDatum = d;
    }

    public Date getGebDatum(){
    	return gebDatum;
    }

    public void setVorname(String n){
    	this.vorname = n;
    }

    public String getVorname(){
    	return vorname;
    }

	public void setNachname(String n){
    	this.nachname = n;
    }

    public String getNachname(){
    	return nachname;
    }    

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
        this.version = version;
    }
}
package de.nst.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class Adresse{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Version
    @Column(name = "version")
    private Integer version;

    @Column(name = "strasse")
    private String strasse;

    @Column(name = "hausnummer")
    private String hausnummer;

    @Column(name = "plz")
    private Integer plz;

    @Column(name = "ort")
    private String ort;

    public void setStrasse(String n){
    	this.strasse = n;
    }

    public String getStrasse(){
    	return strasse;
    }

	public void setHausnummer(String n){
    	this.hausnummer = n;
    }

    public String getHausnummer(){
    	return hausnummer;
    } 

    public void setPLZ(Integer n){
        this.plz = n;
    }

    public Integer getPLZ(){
        return plz;
    }

    public void setOrt(String n){
        this.ort = n;
    }

    public String getOrt(){
        return ort;
    }   
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
        this.version = version;
    }


}
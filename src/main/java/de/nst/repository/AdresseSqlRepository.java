/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.nst.repository;

import de.nst.domain.Adresse;
import de.nst.domain.Person;
import java.util.ArrayList;
import java.util.List;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 *
 * @author stefan
 */
public class AdresseSqlRepository implements AdresseRepositoryI 
{
    private Connection database;
    
    public AdresseSqlRepository()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            
            this.database = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/nst","root","");
        }
        catch (ClassNotFoundException cnfEx)
        {
            // throw cnfEx;
        }
        catch (SQLException sqlEx)
        {
            // throw sqlEx;
        }
    }
    
    @Override
    public int saveAdresse(Adresse a)
    {
        System.out.println("SAVE ADRESSE IN ADRESSE SQL REPO");

        String query;
          
        query = "call insert_adresse('" 
                + a.getStrasse() + "', '" 
                + a.getHausnummer() + "', " 
                + a.getPLZ() + ", '" 
                + a.getOrt() + "');";
        
        ResultSet generatedKeys;
        
        int id = 0;
        
        try
        {   
            Statement statement = this.getDatabase().createStatement();
                    
            statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                
            generatedKeys = statement.getResultSet();

            generatedKeys.next();
            
            id = generatedKeys.getInt(1);          

        }
        catch (SQLException sqlEx)
        {
            System.err.println(sqlEx.getMessage());
        }
        
        return id;
    }

    public Connection getDatabase() 
    {
        return database;
    }

    public void setDatabase(Connection database) 
    {
        this.database = database;
    }

    @Override
    public Adresse findAdresseById(long id) 
    {
        System.out.println("FIND ADRESSE IN ADRESSE SQL REPO");

        String query = "select * from adresse where id =" + id + ";";
        Adresse a = new Adresse();
        
        try{
            Statement statement = getDatabase().createStatement();
            ResultSet rs = statement.executeQuery(query);

            while( rs.next() ) {
                a.setStrasse(rs.getString("strasse"));
                a.setHausnummer(rs.getString("hausnummer"));
                a.setPLZ(rs.getInt("plz"));
                a.setOrt(rs.getString("ort"));
                a.setId(rs.getLong("id"));
                a.setVersion(rs.getInt("version"));
            }
        }
        catch(SQLException e){

        }
        
      
      return a;
    }

    // //###########################################
    // public Person findPersonById(long id) 
    // {
    //     String query = "select * from person where id =" + id + ";";
    //     Person p = new Person();
        
    //     try{
    //         Statement statement = getDatabase().createStatement();
    //         ResultSet rs = statement.executeQuery(query);

    //         while( rs.next() ) {
    //             p.setVorname(rs.getString("vorname"));
    //             p.setNachname(rs.getString("nachname"));
    //             p.setId(rs.getLong("id"));
    //             p.setVersion(rs.getInt("version"));
    //         }
    //     }
    //     catch(SQLException e){

    //     }
        
      
    //   return p;
    // }

    // public List<Person> getAllPeople() {
        
    //     String query = "select * from person";
        
    //     List<Person> personen = new ArrayList<Person>();
        
    //     try{
    //         Statement statement = getDatabase().createStatement();
    //         ResultSet rs = statement.executeQuery(query);

    //         while( rs.next() ) {
    //             Person p = new Person();
    //             p.setVorname(rs.getString("vorname"));
    //             p.setNachname(rs.getString("nachname"));
    //             p.setId(rs.getLong("id"));
    //             p.setVersion(rs.getInt("version"));

    //             personen.add(p);
    //         }
    //     }
    //     catch(SQLException e){

    //     }
        
      
    //   return personen;
    // }

    // //###########################################

    @Override
    public List<Adresse> getAllAdressen() 
    {
        System.out.println("FIND ALL ADRESSE IN ADRESSE SQL REPO");

        String query = "select * from adresse";
        
        List<Adresse> adressen = new ArrayList<Adresse>();
        
        try{
            Statement statement = getDatabase().createStatement();
            ResultSet rs = statement.executeQuery(query);

            while( rs.next() ) {
                Adresse a = new Adresse();
                a.setStrasse(rs.getString("strasse"));
                a.setHausnummer(rs.getString("hausnummer"));
                a.setPLZ(rs.getInt("plz"));
                a.setOrt(rs.getString("ort"));
                a.setId(rs.getLong("id"));
                a.setVersion(rs.getInt("version"));

                adressen.add(a);
            }
        }
        catch(SQLException e){

        }
        
      
      return adressen;
    }
}

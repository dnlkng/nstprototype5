package de.nst.repository;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.nst.domain.Person;

public class PersonRepository implements PersonRepositoryI {

	@PersistenceContext
    private EntityManager em;
	
	/**
	 * @see PersonRepositoryI#findPersonById(java.lang.Long)
	 */
	@Override
	public Person findPersonById(Long id){
		System.out.println("FIND PERSON IN PERSON REPO");
		Person p = em.find(Person.class, id);
		return p;
	}

	@Override
	public void savePerson(Person p) {
		System.out.println("SAVE PERSON IN PERSON REPO");
		em.persist(p);
    	em.flush();
	}

	@Override
	public List<Person> getAllPeople() {
		System.out.println("FIND ALL PERSON IN PERSON REPO");
		List<Person> people = em.createQuery("SELECT o FROM Person o", Person.class).getResultList();
		return people;
	}
}

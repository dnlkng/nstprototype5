package de.nst.repository;

import java.util.List;

import de.nst.domain.Adresse;

public interface AdresseRepositoryI {

	/**
	 * Sucht eine {@link Adresse} mit bestimmter Id.
	 * 
	 * @param id die Id der {@link Adresse}
	 * @return die {@link Adresse}
	 */
	Adresse findAdresseById(long id);
	
	/**
	 * Sucht alle {@link Adresse Adressen}.
	 * 
	 * @return die Liste mit Adressen
	 */
	List<Adresse> getAllAdressen();
	
	/**
	 * Speichert eine {@link Adresse}.
	 * 
	 * @param a die {@link Adresse}
         * @return ID der Adresse.
	 */
	int saveAdresse(Adresse a);
}

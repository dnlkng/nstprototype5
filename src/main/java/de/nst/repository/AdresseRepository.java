package de.nst.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.nst.domain.Adresse;

public class AdresseRepository implements AdresseRepositoryI{

	@PersistenceContext
	EntityManager em;
	
	@Override
	public Adresse findAdresseById(long id) {
		System.out.println("FIND ADRESSE IN ADRESSE REPO");
		Adresse a = em.find(Adresse.class, id);
		return a;
	}

	@Override
	public List<Adresse> getAllAdressen() {
		System.out.println("FIND ALL ADRESSE IN ADRESSE REPO");
		List<Adresse> adressen = em.createQuery("SELECT o FROM Adresse o", Adresse.class).getResultList();
		return adressen;
	}

	@Override
	public int saveAdresse(Adresse a) {
		System.out.println("SAVE ADRESSE IN ADRESSE REPO");
		em.persist(a);	
		return 0;	
	}

}

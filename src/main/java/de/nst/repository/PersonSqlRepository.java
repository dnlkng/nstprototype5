/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.nst.repository;

import de.nst.domain.Person;
import de.nst.repository.AdresseSqlRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author stefan
 */
public class PersonSqlRepository implements PersonRepositoryI
{
    private Connection database;
    
    public PersonSqlRepository()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            
            this.database = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/nst","root","");
        }
        catch (ClassNotFoundException cnfEx)
        {
            // throw cnfEx;
        }
        catch (SQLException sqlEx)
        {
            // throw sqlEx;
        }
    }

    public Connection getDatabase() 
    {
        return database;
    }

    public void setDatabase(Connection database) 
    {
        this.database = database;
    }

    @Override
    public Person findPersonById(Long id) 
    {
        System.out.println("FIND PERSON IN PERSON SQL REPO");

        String query = "select * from person where id =" + id + ";";
        Person p = new Person();
        
        try{
            Statement statement = getDatabase().createStatement();
            ResultSet rs = statement.executeQuery(query);

            while( rs.next() ) {
                p.setVorname(rs.getString("vorname"));
                p.setNachname(rs.getString("nachname"));
                p.setId(rs.getLong("id"));
                p.setVersion(rs.getInt("version"));
            }
        }
        catch(SQLException e){

        }
        
      
      return p;
    }

    @Override
    public void savePerson(Person p) 
    {
        System.out.println("SAVE PERSON IN PERSON SQL REPO");

        String query;

        int adresseId = 0;

        AdresseSqlRepository arep = new AdresseSqlRepository();
        
        try{
            query = "call insert_person('" + 
                p.getVorname() + "', '" + 
                p.getNachname() + "', " + null;

            if (p.getAdressen() == null)
            {
                query += ", " + null + ");";
            }
            else
            {
                adresseId = arep.saveAdresse(p.getAdressen().get(0));
                
                query += ", " + adresseId + ");";
                System.out.println("QUERY: " + query);
            }
            
            Statement statement = this.getDatabase().createStatement();
            
            statement.executeUpdate(query);

            ResultSet generatedKeys = statement.getResultSet();

            generatedKeys.next();
            p.setId(Long.valueOf(generatedKeys.getInt(1)));
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
 
        
    }

    @Override
    public List<Person> getAllPeople() 
    {
        System.out.println("FIND ALL PERSON IN PERSON SQL REPO");

        String query = "select * from person";
        
        List<Person> personen = new ArrayList<Person>();
        
        try{
            Statement statement = getDatabase().createStatement();
            ResultSet rs = statement.executeQuery(query);

            while( rs.next() ) {
                Person p = new Person();
                p.setVorname(rs.getString("vorname"));
                p.setNachname(rs.getString("nachname"));
                p.setId(rs.getLong("id"));
                p.setVersion(rs.getInt("version"));

                personen.add(p);
            }
        }
        catch(SQLException e){

        }
      return personen;
    }
}

package de.nst.repository;

import java.util.List;

import de.nst.domain.Person;

public interface PersonRepositoryI {

	/**
	 * Sucht eine {@link Person} mit der angebenen Id.
	 * 
	 * @param id die Id der Person
	 * @return die Person mit der Id
	 */
	Person findPersonById(Long id);
	
	/**
	 * Speichert eine {@link Person}.
	 * 
	 * @param p die Person
	 */
	void savePerson(Person p);

	/**
	 * Gibt alle Personen zurück.
	 * 
	 * @return eine Liste mit allen Personen
	 */
	List<Person> getAllPeople();
}
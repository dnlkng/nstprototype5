package de.nst.service;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import de.nst.domain.Adresse;
import de.nst.domain.Person;
import de.nst.repository.AdresseRepositoryI;
import de.nst.repository.PersonRepositoryI;

public class PersonService implements PersonServiceI {

	@Autowired
	private PersonRepositoryI personRepo;
	
	@Autowired
	private AdresseRepositoryI adresseRepo;
	
	
	@Override
	public void savePersonWithAdresse(Person p, Adresse a) {
		adresseRepo.saveAdresse(a);
		personRepo.savePerson(p);
	}

	@Override
	public Person findPersonById(Long id){
		return personRepo.findPersonById(id);
	}

	@Override
	public List<Person> getAllPeople(){
		return personRepo.getAllPeople();
	}

}

package de.nst.service;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import de.nst.domain.Adresse;
import de.nst.domain.Person;
import de.nst.repository.AdresseSqlRepository;
import de.nst.repository.PersonSqlRepository;

public class PersonServiceSql implements PersonServiceI {

	private PersonSqlRepository personRepo = new PersonSqlRepository();
	private AdresseSqlRepository adresseRepo = new AdresseSqlRepository();
	
	
	@Override
	public void savePersonWithAdresse(Person p, Adresse a) {
		// adresseRepo.saveAdresse(a); //TODO: Weg???
		personRepo.savePerson(p);
	}

	@Override
	public Person findPersonById(Long id){
		return personRepo.findPersonById(id);
	}

	@Override
	public List<Person> getAllPeople(){
		return personRepo.getAllPeople();
	}

}

package de.nst.service;

import java.util.List;

import de.nst.domain.Adresse;
import de.nst.domain.Person;

public interface PersonServiceI {

	/**
	 * Speichert eine {@link Person} und deren {@link Adresse}.
	 * 
	 * @param p die Person
	 * @param a die Adresse
	 */
	void savePersonWithAdresse(Person p, Adresse a);

    /**
     * Sucht eine {@link Person}.
     * 
     * @param id die Id der Person
     */
    Person findPersonById(Long id);

    /**
     * Gibt alle Personen zurück.
     * 
     * @return eine Liste mit allen Personen
     */
    List<Person> getAllPeople();
}

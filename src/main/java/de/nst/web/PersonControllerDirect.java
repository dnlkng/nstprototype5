package de.nst.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.nst.domain.Adresse;
import de.nst.domain.Person;

import de.nst.repository.AdresseSqlRepository;
import de.nst.repository.PersonSqlRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

@RequestMapping("/direct/person")
@Controller
public class PersonControllerDirect {

    private Connection database;
    private Logger logger = Logger.getLogger(PersonControllerDirect.class);

    private List<Long> valuesSave = new ArrayList<Long>();
    private List<Long> valuesFindSingle = new ArrayList<Long>();
    private List<Long> valuesGetAll = new ArrayList<Long>();

    @PostConstruct
    public void populateMovieCache() {
        try{
            database = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/nst","root","");
        }
        catch(SQLException e){

        }
    }

    @Transactional
    @RequestMapping(value="/log", method = RequestMethod.GET, produces = "text/html")
    public String log(){

        //Gibt die Zeiten der einzelnen Personenaufrufe aus
        //und zeigt danach alle Personen an.




        logger.info("Values for find single entry in nano:");

        for(int i=0; i < valuesFindSingle.size(); i++){
            logger.info(valuesFindSingle.toArray()[i]);
        }

        logger.info("Values for save single entry in nano:");

        for(int i=0; i < valuesSave.size(); i++){
            logger.info(valuesSave.toArray()[i]);
        }

        logger.info("Values for find all entry in nano:");

        for(int i=0; i < valuesGetAll.size(); i++){
            logger.info(valuesGetAll.toArray()[i]);
        }

        
        return "redirect:";
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm() {
        return "person/create";
    }

    @Transactional
    @RequestMapping(value="/{personId}", method = RequestMethod.GET, produces = "text/html")
    public ModelAndView getPersonById(@PathVariable String personId){

    	long startTime = System.nanoTime();



        String query = "select * from person where id =" + personId + ";";
        Person p = new Person();
        
        try{
            Statement statement = database.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while( rs.next() ) {
                p.setVorname(rs.getString("vorname"));
                p.setNachname(rs.getString("nachname"));
                p.setId(rs.getLong("id"));
                p.setVersion(rs.getInt("version"));
            }
        }
        catch(SQLException e){

        }



    	long endTime = System.nanoTime();

        valuesFindSingle.add(endTime - startTime);


    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.addObject("vorname", p.getVorname());
    	modelAndView.addObject("nachname", p.getNachname());
    	modelAndView.setViewName("person/show");

    	return modelAndView;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.GET, produces = "text/html")
    public ModelAndView listAllPeople(){

    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName("person/list");

        long startTime = System.nanoTime();
    	// List<Person> people = personService.getAllPeople();

        String query = "select * from person";
        
        List<Person> personen = new ArrayList<Person>();
        
        try{
            Statement statement = database.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while( rs.next() ) {
                Person p = new Person();
                p.setVorname(rs.getString("vorname"));
                p.setNachname(rs.getString("nachname"));
                p.setId(rs.getLong("id"));
                p.setVersion(rs.getInt("version"));

                personen.add(p);
            }
        }
        catch(SQLException e){

        }



        long endTime = System.nanoTime();

        valuesGetAll.add(endTime - startTime);

    	modelAndView.addObject("people", personen);

    	return modelAndView;
    }

	@Transactional    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public ModelAndView handle(@RequestParam("vorname") String vname, @RequestParam("nachname") String nname, @RequestParam("strasse") String strasse, @RequestParam("nr") String nr, @RequestParam("plz") String plz, @RequestParam("ort") String ort){
		
		Person p = new Person();
		p.setVorname(vname);
		p.setNachname(nname);

		Adresse a = new Adresse();
		
		//Straße:
		if(strasse != null && nr != null && plz != null && ort != null){
			if(strasse.length() > 0 && nr.length() > 0 && plz.length() > 0 && ort.length() > 0){

						a.setStrasse(strasse);
						a.setHausnummer(nr);
						a.setPLZ(Integer.valueOf(plz));
						a.setOrt(ort);

						List<Adresse> adressen = new ArrayList<Adresse>();
						adressen.add(a);

                        // sqlRepo.saveAdresse(a);

						p.setAdressen(adressen);

						logger.info("Adresse wurde angelegt.");
			}
		}

        long startTime = System.nanoTime();
        // personService.savePersonWithAdresse(p, a);
        // persSqlRepo.savePerson(p);


        String query;
        Statement statement;
        int adresseId = 0;

        AdresseSqlRepository arep = new AdresseSqlRepository();
        
        try{
            query = "call insert_person('" + 
                p.getVorname() + "', '" + 
                p.getNachname() + "', " + null;

            if (p.getAdressen() == null){
                query += ", " + null + ");";
            }
            else{
                // adresseId = arep.saveAdresse(p.getAdressen().get(0));
                
                // query += ", " + adresseId + ");";
                // System.out.println("QUERY: " + query);


                String adressQuery;
          
                adressQuery = "call insert_adresse('" 
                + a.getStrasse() + "', '" 
                + a.getHausnummer() + "', " 
                + a.getPLZ() + ", '" 
                + a.getOrt() + "');";
        
                ResultSet generatedKeys;
                
                int id = 0;
                
                try{   
                    statement = database.createStatement();
                            
                    statement.executeUpdate(adressQuery, Statement.RETURN_GENERATED_KEYS);
                        
                    generatedKeys = statement.getResultSet();

                    generatedKeys.next();
                    
                    id = generatedKeys.getInt(1);          

                }
                catch (SQLException sqlEx){
                    System.err.println(sqlEx.getMessage());
                }
                
                adresseId = id;
            }
            
            // Statement statement = database.createStatement();
            statement = database.createStatement();
            
            statement.executeUpdate(query);

            ResultSet generatedKeys = statement.getResultSet();

            generatedKeys.next();
            p.setId(Long.valueOf(generatedKeys.getInt(1)));
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }






        long endTime = System.nanoTime();

        valuesSave.add(endTime - startTime);

    	logger.info("Person saved in " + (endTime - startTime) + "ms");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:person/" + p.getId());

		return modelAndView;
	}

}

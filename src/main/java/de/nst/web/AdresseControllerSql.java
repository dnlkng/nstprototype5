package de.nst.web;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.nst.domain.Adresse;

@RequestMapping("/sql/adresse")
@Controller
public class AdresseControllerSql {

	@PersistenceContext
    EntityManager em;

	
    @Transactional
    @RequestMapping(method = RequestMethod.GET, produces = "text/html")
    public ModelAndView listAllAddresses(){

    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName("address/list");

    	List<Adresse> addresses = em.createQuery("SELECT o FROM Adresse o", Adresse.class).getResultList();

    	modelAndView.addObject("adressen", addresses);

    	return modelAndView;
    }

    @Transactional
    @RequestMapping(value="/{strassenname}", method = RequestMethod.GET, produces = "text/html")
    public ModelAndView createTest(@PathVariable String strassenname){

        Adresse a = new Adresse();
        a.setStrasse(strassenname);
        em.persist(a);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("address/list");

        List<Adresse> addresses = em.createQuery("SELECT o FROM Adresse o", Adresse.class).getResultList();
        System.out.println("Size: " + addresses.size() );
        modelAndView.addObject("adressen", addresses);

        return modelAndView;
    }
	/*@Transactional    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public ModelAndView handle(@RequestParam("vorname") String vname, @RequestParam("nachname") String nname){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("person/test");
		modelAndView.addObject("vorname", vname);
		modelAndView.addObject("nachname", nname);

		long count =em.createQuery("SELECT COUNT(o) FROM Tier o", Long.class).getSingleResult();
		System.out.println("Count of Tiers: " + count);

		Person p = new Person();
		p.setVorname(vname);
		p.setNachname(nname);
		em.persist(p);
		em.flush();

		System.out.println("postmethod called");
		return modelAndView;
	}*/

}

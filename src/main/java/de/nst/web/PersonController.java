package de.nst.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.nst.domain.Adresse;
import de.nst.domain.Person;
import de.nst.repository.PersonRepositoryI;
import de.nst.service.PersonServiceI;

@RequestMapping("/person")
@Controller
public class PersonController {
	
	@Autowired
	private PersonServiceI personService;

    private Logger logger = Logger.getLogger(PersonController.class);

    private List<Long> valuesSave = new ArrayList<Long>();
    private List<Long> valuesFindSingle = new ArrayList<Long>();
    private List<Long> valuesGetAll = new ArrayList<Long>();

    @Transactional
    @RequestMapping(value="/log", method = RequestMethod.GET, produces = "text/html")
    public String log(){

        //Gibt die Zeiten der einzelnen Personenaufrufe aus
        //und zeigt danach alle Personen an.

        logger.info("Values for find single entry in nano:");
        for(int i=0; i < valuesFindSingle.size(); i++){
            logger.info(valuesFindSingle.toArray()[i]);
        }

        logger.info("Values for save single entry in nano:");
        for(int i=0; i < valuesSave.size(); i++){
            logger.info(valuesSave.toArray()[i]);
        }

        logger.info("Values for find all entry in nano:");
        for(int i=0; i < valuesGetAll.size(); i++){
            logger.info(valuesGetAll.toArray()[i]);
        }

        return "redirect:";
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm() {
        return "person/create";
    }

    @Transactional
    @RequestMapping(value="/{personId}", method = RequestMethod.GET, produces = "text/html")
    public ModelAndView getPersonById(@PathVariable String personId){

    	long startTime = System.nanoTime();
    	Person p = personService.findPersonById(Long.valueOf(personId));
    	long endTime = System.nanoTime();

        valuesFindSingle.add(endTime - startTime);

    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.addObject("vorname", p.getVorname());
    	modelAndView.addObject("nachname", p.getNachname());
    	modelAndView.setViewName("person/show");

    	return modelAndView;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.GET, produces = "text/html")
    public ModelAndView listAllPeople(){

    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName("person/list");

        long startTime = System.nanoTime();
    	List<Person> people = personService.getAllPeople();
        long endTime = System.nanoTime();

        valuesGetAll.add(endTime - startTime);

    	modelAndView.addObject("people", people);

    	return modelAndView;
    }

	@Transactional    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public ModelAndView handle(@RequestParam("vorname") String vname, @RequestParam("nachname") String nname, @RequestParam("strasse") String strasse, @RequestParam("nr") String nr, @RequestParam("plz") String plz, @RequestParam("ort") String ort){
		
		Person p = new Person();
		p.setVorname(vname);
		p.setNachname(nname);

		Adresse a = new Adresse();
		
		//Straße:
		if(strasse != null && nr != null && plz != null && ort != null){
			if(strasse.length() > 0 && nr.length() > 0 && plz.length() > 0 && ort.length() > 0){

						a.setStrasse(strasse);
						a.setHausnummer(nr);
						a.setPLZ(Integer.valueOf(plz));
						a.setOrt(ort);

						List<Adresse> adressen = new ArrayList<Adresse>();
						adressen.add(a);

						p.setAdressen(adressen);
			}
		}

        long startTime = System.nanoTime();
        personService.savePersonWithAdresse(p, a);
        long endTime = System.nanoTime();

        valuesSave.add(endTime - startTime);

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:person/" + p.getId());

		return modelAndView;
	}

}
